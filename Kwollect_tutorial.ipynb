{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e51efe91-2ca8-4d4a-a58a-a8dd56381660",
   "metadata": {},
   "source": [
    "### Note\n",
    "\n",
    "You are in the notebook version of the [Energy consumption monitoring tutorial wiki page](https://www.grid5000.fr/w/Energy_consumption_monitoring_tutorial), intended to ease the usage of provided examples. Please ensure you are running this notebook from a `reserved node` (not on a `frontend`). Some adaptations are provided to the original wiki (notably to run some bash code in `ipython3` notebook cells and features to make the execution of this notebook possible on any cluster/node), but should not have any incidence on the core content of the original wiki page."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "6993f1d3-825a-4c9d-8232-ac4154c8bcfd",
   "metadata": {},
   "source": [
    "# Energy consumption monitoring tutorial\n",
    "\n",
    "## Introduction\n",
    "\n",
    "This tutorial will show how to monitor energy on Grid'5000.\n",
    "\n",
    "On Lyon, Grenoble and Nancy sites, special devices (called \"wattmeter\") allow fine grained measurements (50 measure each second, with sub-watt resolution). In addition, electrical power consumption of nodes may sometimes be retrieved from their Power Distribution Units (PDU), the device which supply them with electrical power. While less precise, many clusters also have monitoring capabilities provided by their \"BMC\" (server specific adminstration board).\n",
    "\n",
    "Grid'5000 uses the [Kwollect tool](https://www.grid5000.fr/w/Monitoring_Using_Kwollect) to provide a convenient and consistent way to monitor energy consumption, and other monitoring metrics, in experiments.\n",
    "\n",
    "In the tutorial, you will learn how to retrieve energy consumed by Grid'5000 nodes. The power consumption will be studied under various workload scenario and combinations of CPU energy saving parameters (P-State, C-State, etc.).\n",
    "\n",
    "This tutorial requires a basic knowledge of Grid'5000 usage (i.e. having completed [Getting Started](https://www.grid5000.fr/w/Getting_Started) tutorial). \n",
    "\n",
    "## Retrieving energy consumption data\n",
    "\n",
    "### Using Kwollect\n",
    "\n",
    "[Kwollect](https://gitlab.inria.fr/grid5000/kwollect) is a monitoring tool focus on environmental metrics such as electrical consumption. On Grid'5000, it permanently collects metrics on every nodes, network equipment, PDUs and Wattmeter and store them in a long term storage. Collected metrics are exposed to users through Grid'5000 API and a visualization dashboard based on Grafana.\n",
    "\n",
    "Kwollect usage on Grid'5000 has a [dedicated documentation](https://www.grid5000.fr/w/Monitoring_Using_Kwollect).\n",
    "\n",
    "The visualization interface can display \"live\" view of energy being consumed by a node or by a group of nodes inside an OAR reservation. However for experimenting purpose, it may be more useful to get access to raw values available using APIs. It is available at `https://api.grid5000.fr/stable/sites/<site>/metrics/dashboard` (for instance: [Lyon](https://api.grid5000.fr/stable/sites/lyon/metrics/dashboard))\n",
    "\n",
    "The Grid'5000 API is particularly suited to get data for measures performed in the past. For instance, to get the power consumed by nodes \"taurus-1\" and \"taurus-3\" as reported by wattmeters, at Lyon, between 10:35 and 10:40 on March, 21, use the URL:\n",
    "\n",
    "https://api.grid5000.fr/stable/sites/lyon/metrics?nodes=taurus-1,taurus-3&metrics=wattmetre_power_watt&start_time=2021-03-21T10:35&end_time=2021-03-21T10:40\n",
    "\n",
    "(beware if using this URL on a command line, quote it to avoid '&' being interpreted as the job control operator to put the command in background)\n",
    "\n",
    "Note that the time range provided should be of the same order of magnitude as a typical job duration (e.g. no more than a few hours). Otherwise, requests must be serialized.\n",
    "\n",
    "Other power consumption metrics may be available on clusters, such as: pdu_outlet_power_watt and bmc_node_power_watt. See [Kwollect documentation](https://www.grid5000.fr/w/Monitoring_Using_Kwollect#Metrics_available_in_Grid.275000) for full list of metrics.\n",
    "\n",
    "Note that by default, wattmetre values are collected every one second using Kwollect (it stores the average of the 50 measurements performed over one second). If you need the 50 measurments every second, you must tell Kwollect to enable wattmetre's high frequency monitoring for your job at submission time:\n",
    "```\n",
    "$ oarsub -I -t monitor='wattmetre_power_watt'\n",
    "```\n",
    "\n",
    "### Raw wattmeters data\n",
    "\n",
    "Lyon, Grenoble and Nancy sites provide dedicated devices, called \"wattmetres\", to monitor energy consumption (more information in [Grenoble:Wattmetre](https://www.grid5000.fr/w/Grenoble:Wattmetre), [Lyon:Wattmetre](https://www.grid5000.fr/w/Lyon:Wattmetre), [Nancy:Wattmetre](https://www.grid5000.fr/w/Nancy:Wattmetre)). Nodes may be powered by 1 (e.g. clusters in Lyon, gros cluster in Nancy, troll cluster in Grenoble) or 2 supply units (e.g. yeti cluster in Grenoble). All power supply units are measured individually by the wattmeters, providing the electrical power consumed every 20 milliseconds (50hz), with a precision of 0.1 watts.\n",
    "\n",
    "As seen above, the wattmetres values are provided by Kwollect. In addition, \"raw\" data collected by wattmetres, including the 50 measurements made each second, is stored in CSV files and available from Grid5000 network to download at: http://wattmetre.lyon.grid5000.fr/data, http://wattmetre.grenoble.grid5000.fr/data and http://wattmetre.nancy.grid5000.fr/data. Downloading raw data files might be more appropriate than using Kwollect to get monitoring values over a large period of time.\n",
    "\n",
    "For each wattmetre, a new file is recorded at the beginning of every hour (files from past hours are kept compressed). The file name format is \"power.csv.<YYYY-MM-DD>T<HH>\", where <YYYY-MM-DD> is the date of the recording and <HH> the hour when it begun.\n",
    "\n",
    "Here is the meaning of columns in the CSV files:\n",
    "\n",
    "- 1st and 2nd columns: Debugging information (these columns will be removed in the future)\n",
    "- 3rd column: Timestamp when the measure was performed (as number of seconds and nano-seconds since 00:00:00 1970-01-01 UTC).\n",
    "- 4th column: Must be \"OK\" if the measure has correctly been performed, other it should be discarded\n",
    "- From 5th column to the last: Electrical power consumed for each wattmetre's port. The 5th column shows value for port number 0, the 6th for port number 1, etc. (beware that for yeti cluster in Grenoble, several ports are used to supply a single node). Sometimes the value may be missing for a particular port. It means that wattmetre was not able to compute it correctly.\n",
    "\n",
    "The mapping between wattmetres' ports and Grid'5000 nodes is available in the Reference API. For instance, nodes connected to \"wattmetre1\" at Lyon are described at:\n",
    "\n",
    "https://api.grid5000.fr/stable/sites/lyon/pdus/wattmetre1.json\n",
    "\n",
    "under \"ports\" section, and wattmetre and port number connected to \"taurus-1\" node is available at:\n",
    "\n",
    "https://api.grid5000.fr/stable/sites/lyon/clusters/taurus/nodes/taurus-1.json\n",
    "\n",
    "under \"pdu\" section.\n",
    "\n",
    "### Intel RAPL data\n",
    "\n",
    "Due to [security reasons](https://cdn.kernel.org/pub/linux/kernel/v4.x/ChangeLog-4.19.157), to be able to read power data using the powercap interface, you need to authorize non-privileged user access in `/sys/class/powercap/intel-rapl*/*/energy_uj` on the node.\n",
    "\n",
    "For example with sudo-g5k in the std environment:\n",
    "```\n",
    "$ sudo-g5k chmod 444 /sys/class/powercap/intel-rapl/intel-rapl*/energy_uj\n",
    "```\n",
    "\n",
    "From this notebook you can run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79d0c260-bee0-4da8-9c2f-578747b4643d",
   "metadata": {},
   "outputs": [],
   "source": [
    "from os import system\n",
    "\n",
    "system(\"sudo-g5k chmod 444 /sys/class/powercap/intel-rapl/intel-rapl*/energy_uj\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50458077-11f0-4716-ad2e-082e3635d1be",
   "metadata": {},
   "source": [
    "## Power consumption under different workloads\n",
    "\n",
    "In the previous section, we have learned how to retrieve energy consumption information. In this part, we will illustrate these monitoring features in an example scenario: We will show how energy consumption evolves under different workload, and the impact of various CPU's energy-related parameters.\n",
    "\n",
    "### Preliminary remarks\n",
    "\n",
    "- In the examples given in this part, we will use the Kwollect through the Grid'5000 API.\n",
    "- In this scenario, you need to reserve one node and install some additional tools inside it. As you will require to be root, you can use sudo-g5k to get sudo rights, or use kadeploy to deploy your own environment. Then, you can install the required tools with the following command:\n",
    "```\n",
    "apt update && apt install linux-cpupower sysbench\n",
    "```\n",
    "\n",
    "From this notebook you can run (using [sudo-g5k](https://www.grid5000.fr/w/Sudo-g5k)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ccc2f9d7-0f0c-48fc-8617-46a293cd49bb",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from os import system\n",
    "\n",
    "system(\"sudo-g5k apt update && sudo-g5k apt install -y linux-cpupower sysbench python3-requests\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "04ae8938-5595-49ed-886c-6e67d66482d5",
   "metadata": {},
   "source": [
    "### Workload examples\n",
    "\n",
    "We will consider 3 different workloads:\n",
    "\n",
    "- **Idle**: Nothing is done of the machine\n",
    "- **CPU Intensive, mono-threaded**: The machine run a CPU intensive application on one of its core. We will use the \"sysbench\" benchmarking tool to mimic this workload, invoked with:\n",
    "```bash\n",
    "sysbench --test=cpu --cpu-max-prime=50000 --num-threads=1 run\n",
    "```\n",
    "\n",
    "- **CPU Intensive, multi-threaded**: The machine run a CPU intensive application on all of its core. We will also use \"sysbench\", invoked with:\n",
    "```bash\n",
    "NUM_THREADS=$(getconf _NPROCESSORS_ONLN)\n",
    "sysbench --test=cpu --cpu-max-prime=50000 --num-threads=$NUM_THREADS run\n",
    "```\n",
    "(`$NUM_THREAD` is the number of threads to run, we will use the number of cores avaible on the node we use)\n",
    "\n",
    "### Impact of CPU parameters\n",
    "\n",
    "Several CPU parameters tries are available to lower energy consumed under certain workload. In particular:\n",
    "\n",
    "- C-States configuration is the ability for processors and cores to go to energy saver \"sleep states\" when not being used.\n",
    "- P-States policy dynamically adjusts voltage and frequency of cores to fit workload\n",
    "- Turboboost allows cores to run at higher frequency while they stay under temperature specification limits.\n",
    "\n",
    "In this example scenario, we will investigate two different C-States configuration : Partially enabled (the maximum authorized sleep state is C1, this is the default on Grid'5000) and fully enabled (all sleep states are allowed, the deeper sleep state on modern machine is usually C6). To change the maximum allowed sleep state allowed, we will use cpupower command. For instance, to allow all sleep states available, use:\n",
    "```\n",
    "cpupower idle-set -E\n",
    "```\n",
    "To disable sleep states that would require more than 20 microseconds to be awakened from it (i.e. disable C-States higher than C1):\n",
    "```\n",
    "cpupower idle-set -D 20\n",
    "```\n",
    "We will also study the impact of turboboost by enabling (which is the default on Grid'5000) or disabling it. To disable turboboost, the following command must be used:\n",
    "```\n",
    "echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo\n",
    "```\n",
    "\n",
    "\n",
    "\n",
    "### Scenario implementation\n",
    "\n",
    "We propose to study following metrics:\n",
    "\n",
    "- Average electrical power required to run workload\n",
    "- Time needed to run CPU workload\n",
    "- The ops per watt value, i.e. the average number of operation per second and per Watt, a metric reflecting the \"energy efficiency\" of machines\n",
    "\n",
    "The average electrical power required to run the workload is the amount of electrical energy spent during its execution divided by the execution time. Its value can be approximated as the average of the power values which have been monitored during execution.\n",
    "\n",
    "Using your favorite programming language, write a function that queries the Grid'5000 API to return the average power used by a Grid'5000 node between two dates (as Unix timestamps).\n",
    "\n",
    "#### Notebook set up\n",
    "\n",
    "Fetch information about the current notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbbd9916-8ffb-427a-9b48-c248d1510b0e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import socket\n",
    "import os\n",
    "\n",
    "hostname = socket.gethostname().split('.')\n",
    "\n",
    "jobid = os.environ[\"OAR_JOBID\"]\n",
    "node = hostname[0]\n",
    "cluster = node.split('-')[0]\n",
    "site = hostname[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b1e009e-82f0-4a75-8cb2-ad84852f1d69",
   "metadata": {},
   "source": [
    "Get metrics available for the current node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a54530c6-4117-46d5-9051-92fcd1b7effe",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import requests\n",
    "\n",
    "data = requests.get(\"https://api.grid5000.fr/stable/sites/%s/clusters/%s\" % (site, cluster), verify=False).json()\n",
    "for metric in data[\"metrics\"]:\n",
    "    if \"only_for\" in metric.keys():\n",
    "        if node in metric[\"only_for\"]:\n",
    "            print(metric[\"name\"])\n",
    "    else:\n",
    "        print(metric[\"name\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19ce9651-cb5b-435c-9a68-4a8f8f7498b4",
   "metadata": {},
   "source": [
    "If you want, you can modify the default power measurement according to what is available on the current node:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5984c7d-8314-4d66-86f1-196af97470b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "default_metric=\"bmc_node_power_watt\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc61577c-80cc-4d36-964e-db5a0c80f43d",
   "metadata": {},
   "source": [
    "You can access the dashboard for the job running the current notebook at the following url:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0799f77f-b70b-4d25-b2e4-8cb69771b88c",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"https://api.grid5000.fr/stable/sites/%s/metrics/dashboard/d/kwollect/kwollect-metrics?var-job_str=%s&var-device_id=%s&var-metric_id=%s\" % (site, jobid, node, default_metric)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "415511ea-9b20-4ee8-8a98-2a9375bf7707",
   "metadata": {},
   "source": [
    "#### Solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4686b0a-08db-49a0-ba37-59b1a8035287",
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "# you may need to install requests for python3 with sudo-g5k apt install python3-requests\n",
    "from statistics import mean\n",
    "\n",
    "def get_power(node, site, start, stop, metric=default_metric):\n",
    "    url = \"https://api.grid5000.fr/stable/sites/%s/metrics?metrics=%s&nodes=%s&start_time=%s&end_time=%s\" \\\n",
    "            % (site, metric, node, int(start), int(stop))\n",
    "    data = requests.get(url, verify=False).json()\n",
    "    return sum(item['value'] for item in data)/len(data)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "718c4d0b-ed86-4195-b4b7-d60db335f7c4",
   "metadata": {},
   "source": [
    "#### **Idle workload**\n",
    "\n",
    "First, we are going to investigate how C-States influence energy consumed when machine is idle.\n",
    "\n",
    "Turn off C-States and leave the machine idle. What is the energy consumed during the last ten seconds ? Turn on C-States and repeat. How many Watts have been saved by C-States ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "495f1b93-4992-4f0b-9930-0815edcdf899",
   "metadata": {},
   "outputs": [],
   "source": [
    "from os import system\n",
    "from time import sleep, time\n",
    "\n",
    "# Turn off C-States\n",
    "system(\"sudo cpupower idle-set -D0\")\n",
    "sleep(20)\n",
    "power_cstate_off = get_power(node, site, time()-20, time()-10)\n",
    "\n",
    "# Turn on C-States\n",
    "system(\"sudo cpupower idle-set -E\")\n",
    "sleep(20)\n",
    "power_cstate_on = get_power(node, site, time()-20, time()-10)\n",
    "\n",
    "print(power_cstate_off - power_cstate_on)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd22ef73-dda7-4244-b2d6-06a6ad8c581c",
   "metadata": {},
   "source": [
    "#### **CPU intensive, mono-threaded, workload**\n",
    "\n",
    "We are now going to run CPU intensive workload and see how CPU parameters influence the average power consumption but also the time spent to execute the workload.\n",
    "\n",
    "For instance, turn off C-States and Turboboost and measure the workload runtime, and then get the average power consumed. Repeat with C-States turned on, with or without Turboboost. Which combination consumes less power ? Which one runs faster ? has the best ops/watt ratio ? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0ebc7b7-0083-421e-8e9a-12e720ad4d1f",
   "metadata": {},
   "outputs": [],
   "source": [
    "from os import system\n",
    "from time import sleep, time\n",
    "\n",
    "# Turn off C-States and Turboboost\n",
    "system(\"sudo cpupower idle-set -D0\")\n",
    "system(\"echo 1 | sudo-g5k tee /sys/devices/system/cpu/intel_pstate/no_turbo\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=20000 run\")\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_1 = \"C-States OFF, Turbo OFF, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "\n",
    "# Turn on C-States\n",
    "system(\"sudo cpupower idle-set -E\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=20000 run\")\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_2 = \"C-States ON, Turbo OFF, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "\n",
    "# Turn on Turboboost\n",
    "system(\"echo 0 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=20000 run\")\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_3 = \"C-States ON, Turbo ON, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "# Print results\n",
    "print(result_1)\n",
    "print(result_2)\n",
    "print(result_3)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4207bdbc-e0c3-4bc3-bb16-27346790a2a5",
   "metadata": {},
   "source": [
    "#### **CPU intensive, multi-threaded, workload**\n",
    "\n",
    "We are now going to repeat the same experiment with a multi-threaded workload, running on every cores the machine has. Run the workload with or without C-States and Turboboost activated and observe runtime and power consumed. What can you say abount the influence of CPU parameters on multi-threaded, CPU intensive workload ? Is running multi-threaded is more energy efficient ? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4adb91e0-a655-4ebd-9886-7604e37d3ff3",
   "metadata": {},
   "outputs": [],
   "source": [
    "from os import system\n",
    "from time import sleep, time\n",
    "import requests\n",
    "\n",
    "# Get core count\n",
    "core_count = requests.get(\n",
    "                 \"https://api.grid5000.fr/stable/sites/%s/clusters/%s/nodes/%s\" % (site, cluster, node),\n",
    "                 verify=False\n",
    "                 ).json()['architecture']['nb_cores']\n",
    "\n",
    "# Turn off C-States and Turboboost\n",
    "system(\"sudo cpupower idle-set -D0\")\n",
    "system(\"echo 1 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=50000 --num-threads=%s run\" % core_count)\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_1 = \"C-States OFF, Turbo OFF, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "\n",
    "# Turn on C-States\n",
    "system(\"sudo cpupower idle-set -E\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=50000 --num-threads=%s run\" % core_count)\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_2 = \"C-States ON, Turbo OFF, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "\n",
    "# Turn on Turboboost\n",
    "system(\"echo 0 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo\")\n",
    "\n",
    "# Run workload\n",
    "start = time()\n",
    "system(\"sysbench --test=cpu --cpu-max-prime=50000 --num-threads=%s run\" % core_count)\n",
    "stop = time()\n",
    "\n",
    "# Get results\n",
    "sleep(5)\n",
    "power = get_power(node, site, start, stop)\n",
    "result_3 = \"C-States ON, Turbo ON, Duration: %f, Power: %f\" % (stop-start, power)\n",
    "\n",
    "# Print results\n",
    "print(result_1)\n",
    "print(result_2)\n",
    "print(result_3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "299606fd-8c66-445b-a6ef-ac84d779e473",
   "metadata": {},
   "source": [
    "## Going further\n",
    "\n",
    "- The various monitoring devices used in Grid'5000 are presented in this page: [Power Monitoring Devices](https://www.grid5000.fr/w/Power_Monitoring_Devices)\n",
    "- More details about Grid'5000 monitoring capabilities with Kwollect are available at: [Monitoring Using Kwollect](https://www.grid5000.fr/w/Monitoring_Using_Kwollect)\n",
    "- More information about modifying CPU parameters on Grid'5000: [CPU parameters](https://www.grid5000.fr/w/CPU_parameters)\n",
    "- More information about Grid'5000 API: [API](https://www.grid5000.fr/w/API)\n",
    "- For more experiment scripting in Python, see [Execo Practical Session](https://www.grid5000.fr/w/Execo_Practical_Session)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "83e3a0ee-30dd-4a6b-b36a-8d2c563b064c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
