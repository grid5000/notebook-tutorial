# Notebook Tutorial

This project contain tutorial and demo notebooks to be used on the Grid'5000 platform.  
Go to https://intranet.grid5000.fr/notebooks to start a notebooks server on Grid'5000.  
More info at https://www.grid5000.fr/w/Notebooks .  


* Notebook\_demo.ipynb : Basic intro to notebooks 
  * Demonstrates controling an experiment 
  * Resource reservation using python-grid5000 
  * To be executed on frontends
* Kwollect\_tutorial.ipynb : Energy consumption monitoring tutorial
  * Reflects the associated [wiki page](https://www.grid5000.fr/w/Energy_consumption_monitoring_tutorial)
  * Demonstrates the usage of [sudo-g5k](https://www.grid5000.fr/w/Sudo-g5k) in
    a notebook
  * Gets information in Python about the current job
  * Performs requests against api.grid5000.fr to get power metrics in various
    C-States and Turboboost configurations
  * To be executed on reserved nodes
